import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from 'react-bootstrap';
import axios from 'axios';
import { Route, Router } from 'react-router';
import { connect } from 'react-redux';


class App extends Component {

  constructor(props) {
    super(props);

    this.state = { value: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.getAccount();

    // var params = new URLSearchParams();
    // params.append('param1', this.state.value);
    // axios.post('/foo', params);
  }

  getAccount() {
  }

  render() {
    return (
      <div className="App">
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    account: state.account
  };
};


// const mapDispatchToState = dispatch => {
//   return {
//     getAccount: () => {
//       return dispatch(getAccount());
//     }
//   };
// };

export default connect(mapStateToProps, null)(App)