import { applyMiddleware, createStore } from 'redux';
import promise from 'redux-promise-middleware';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import reducers from './reducers';

const middleware = applyMiddleware(promise(), thunk);

const store = createStore(reducers, middleware);

store.subscribe(() => {
    console.log('store', composeWithDevTools(store.getState()));
})

export default store;