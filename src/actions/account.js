import axios from 'axios'

export function changePassword(oldPassword, newPassword, confirmPassword) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {

            return axios.post(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/changePassword', { oldPassword, newPassword, confirmPassword }, config).then((response) => {
                dispatch({ type: 'CHANGE_PASSWORD_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'CHANGE_PASSWORD_REJECTED', payload: error });
                reject(error);
            })

        });
    }
}

export function createAccount(account) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {

            return axios.post(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/create', account, config).then((response) => {
                dispatch({ type: 'CREATE_ACCOUNT_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'CREATE_ACCOUNT_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function editAccount(account) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.put(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/edit', account, config).then((response) => {
                dispatch({ type: 'EDIT_ACCOUNT_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'EDIT_ACCOUNT_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function getAccount(id) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.get(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/user/' + id, config).then((response) => {
                dispatch({ type: 'GET_ACCOUNT_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'GET_ACCOUNT_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}


export function getProfile() {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.get(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/profile', config).then((response) => {
                dispatch({ type: 'GET_ACCOUNT_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'GET_ACCOUNT_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function deleteAccount(id) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.delete(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/user/' + id, config).then((response) => {
                dispatch({ type: 'DELETE_ACCOUNT_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'DELETE_ACCOUNT_REJECTED', payload: error });
                reject(error);
            })
        });

    }
}

export function editAccountRoles(id, roles) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.put(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/user/' + id + '/roles', roles, config).then((response) => {
                dispatch({ type: 'EDIT_ACCOUNT_ROLES_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'EDIT_ACCOUNT_ROLES_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function getAccountRoles(id, roles) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.get(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/user/' + id + '/roles', config).then((response) => {
                dispatch({ type: 'GET_ACCOUNT_ROLES_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'GET_ACCOUNT_ROLES_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}