import axios from 'axios'

export function fetchRoles() {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }
        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'api/roles', config).then((response) => {
            dispatch({ type: 'FETCH_ROLES_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_ROLES_REJECTED', payload: error })
        })
    }
}