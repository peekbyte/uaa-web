import axios from 'axios'

export function auth(userName, passsword) {
    return function (dispatch) {
        const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }

        const formData = 'grant_type=password&username=' + userName + '&password=' + passsword;
        return new Promise((resolve, reject) => {
            axios.post(process.env.REACT_APP_ENDPOINT_URL + 'oauth/token', formData, config).then((response) => {
                dispatch({ type: 'LOGIN_SUCCEED', payload: response.data });
                sessionStorage.setItem('access-token', response.data.access_token);

                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'LOGIN_REJECTED', payload: error })

                reject(error);
            })
        });
    }
}
