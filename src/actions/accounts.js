import axios from 'axios';

export function fetchAccounts(userName = '', firstName = '', lastName = '') {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        const q = 'userName=' + userName +'&firstName=' + firstName + '&lastName=' + lastName;

        axios.get(process.env.REACT_APP_ENDPOINT_URL + 'api/accounts/users?' + q , config).then((response) => {
            dispatch({ type: 'FETCH_ACCOUNTS_FULFILLED', payload: response.data })
        }).catch((error) => {
            dispatch({ type: 'FETCH_ACCOUNTS_REJECTED', payload: error })
        })
    }
}
