import axios from 'axios'

export function createRole(role) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.post(process.env.REACT_APP_ENDPOINT_URL + 'api/roles/create', role, config).then((response) => {
                dispatch({ type: 'CREATE_ROLE_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'CREATE_ROLE_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function editRole(role) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.post(process.env.REACT_APP_ENDPOINT_URL + 'api/roles/edit', role, config).then((response) => {
                dispatch({ type: 'EDIT_ROLE_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'EDIT_ROLE_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function deleteRole(id) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }

        return new Promise((resolve, reject) => {
            return axios.delete(process.env.REACT_APP_ENDPOINT_URL + 'api/roles/' + id, config).then((response) => {
                dispatch({ type: 'DELETE_ROLE_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'DELETE_ROLE_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}

export function getRole(id) {
    return function (dispatch) {
        const token = sessionStorage.getItem('access-token');
        const config = { headers: { 'Authorization': 'Bearer ' + token } }
        return new Promise((resolve, reject) => {
            return axios.get(process.env.REACT_APP_ENDPOINT_URL + 'api/roles/' + id, config).then((response) => {
                dispatch({ type: 'GET_ROLE_SUCCEED', payload: response.data });
                resolve(response.data);
            }).catch((error) => {
                dispatch({ type: 'GET_ROLE_REJECTED', payload: error });
                reject(error);
            })
        });
    }
}