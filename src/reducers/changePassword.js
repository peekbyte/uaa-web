export default function (state = {
    fetched: false,
    fetching: false,
    error: null,
    message: 'dfdfdfd',
    result: []

}, action) {
    switch (action.type) {
        case 'CHANGE_PASSWORD_SUCCEED':
            return { ...state, fetched: true, result: action.payload };
        case 'CHANGE_PASSWORD_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}