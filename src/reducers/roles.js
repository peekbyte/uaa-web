export default function (state = {
    roles: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_ROLES_FULFILLED':
            return { ...state, fetched: true, roles: action.payload };
        case 'FETCH_ROLES_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}