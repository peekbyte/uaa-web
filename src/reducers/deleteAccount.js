export default function (state = {
    fetched: false,
    fetching: false,
    error: null,
    message: '',
    result: {}
}, action) {
    switch (action.type) {
        case 'DELETE_ACCOUNT_SUCCEED':
            return { ...state, fetched: true, result: action.payload };
        case 'DELETE_ACCOUNT_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}