export default function (state = {
    fetched: false,
    fetching: false,
    error: null,
    message: '',
    roles: {}
}, action) {
    switch (action.type) {
        case 'GET_ACCOUNT_ROLES_SUCCEED':
            return { ...state, fetched: true, result: action.payload };
        case 'GET_ACCOUNT_ROLES_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}