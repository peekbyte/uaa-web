export default function (state = {
    fetched: false,
    fetching: false,
    error: false,
    role: {}

}, action) {
    switch (action.type) {
        case 'GET_ROLE_SUCCEED':
            return { ...state, fetched: true, role: action.payload };
        case 'GET_ROLE_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}