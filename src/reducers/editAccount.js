export default function (state = {
    fetched: false,
    fetching: false,
    error: null,
    message: '',
    result: {}
}, action) {
    switch (action.type) {
        case 'EDIT_ACCOUNT_SUCCEED':
            return { ...state, fetched: true, result: action.payload };
        case 'EDIT_ACCOUNT_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}