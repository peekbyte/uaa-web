export default function (state = {
    fetched: false,
    fetching: false,
    error: null,
    message: '',
    result: {}
}, action) {
    switch (action.type) {
        case 'CREATE_ROLE_SUCCEED':
            return { ...state, fetched: true, result: action.payload };
        case 'CREATE_ROLE_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}