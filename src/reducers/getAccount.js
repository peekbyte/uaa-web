export default function (state = {
    fetched: false,
    fetching: false,
    error: null,
    account: {}

}, action) {
    switch (action.type) {
        case 'GET_ACCOUNT_SUCCEED':
            return { ...state, fetched: true, account: action.payload };
        case 'GET_ACCOUNT_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}