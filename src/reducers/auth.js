export default function (state = {
    token: { access_token: window.localStorage.getItem("jwt-token"), expire: '' },
    account: Object.assign({
        firstName: '',
        lastName: ''
    }, JSON.parse(window.localStorage.getItem("account"))),
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'LOGIN_SUCCEED':
            return { ...state, fetched: true, token: action.payload };
        case 'LOGIN_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}