export default function (state = {
    accounts: [],
    fetched: false,
    fetching: false,
    error: null

}, action) {
    switch (action.type) {
        case 'FETCH_ACCOUNTS_FULFILLED':
            return { ...state, fetched: true, accounts: action.payload };
        case 'FETCH_ACCOUNTS_REJECTED':
            return { ...state, fetched: false, error: true };
    }
    return state;
}