import React from 'react';
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import './index.css'
import App from './App'
import Accounts from './components/accounts'
import Login from './components/login'
import ForgotPassword from './components/forgotPassword'
import Register from './components/register'
import Dashboard from './components/dashboard'
import SideBar from './containers/sideBar'
import NavBar from './containers/navBar'
import Footer from './containers/footer'

import registerServiceWorker from './registerServiceWorker'
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link,
    Switch
} from 'react-router-dom';


function isAuthenticated() {
    return sessionStorage.getItem('access-token') != null;
}

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        isAuthenticated() ? (
            <Component {...props} />
        ) : (
                <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                }} />
            )
    )} />
)

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Switch>
                <PrivateRoute path="/dashboard" component={Dashboard} />

                <Route path="/login" component={Login} />
                <Route exact path="/" render={() => (
                    isAuthenticated() ? (<Redirect to="/dashboard/myprofile" />) : (<Redirect to="/login" />)
                )} />
                <Route path="/Forgotpassword" component={ForgotPassword} />
                <Route path="/register" component={Register} />
            </Switch>
        </Router>
    </Provider>
    , document.getElementById('root'));

registerServiceWorker();