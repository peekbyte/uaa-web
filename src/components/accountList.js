import React, { Component } from 'react';

import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';

export default class AccountList extends Component {

    constructor(props) {
        super(props);
    }

    editHandler(e) {
        this.props.editHandler(e);
    }

    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th> ردیف  </th>
                        <th> نام  </th>
                        <th> نام خانوادگی </th>
                        <th> نام کاربری </th>
                        <th> ایمیل </th>
                        <th className="th-operation"> عملیات </th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.accounts.map((item) => {
                        return (
                            <tr key={item.id} >
                                <td> 1 </td>
                                <td> {item.firstName} </td>
                                <td> {item.lastName} </td>
                                <td> {item.userName} </td>
                                <td> {item.email} </td>
                                <td className="btn-operation">
                                    <Link className="btn green-jungle" to={'account/' + item.id + '/profile'}>
                                        <span className="ladda-label">
                                            <i className="icon-note"></i> ویرایش</span>
                                        <span className="ladda-spinner"></span>
                                    </Link>

                                    <button type="button" onClick={() => { this.props.deleteAccount(item.id) }} className="btn  red-soft " data-style="expand-up">
                                        <span className="ladda-label">
                                            <i className="icon-trash"></i> حذف</span>
                                        <span className="ladda-spinner"></span></button>
                                </td>
                            </tr>
                        )
                    }
                    )}
                </tbody>
            </table>
        )
    }
}