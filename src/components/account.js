import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchAccounts } from '../actions/accounts';
import AccountForm from './accountForm';
import AccountList from './accountList';
import ChangePassword from './changePassword';
import AccountRoles from './accountRoles';
import AccountSearch from './accountSearch';

import { fetchRoles } from '../actions/roles';

import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';

class Account extends Component {

    constructor(props) {
        super(props);
        this.props.fetchRoles();
        this.state = { id: '', globalError: '', success: '' };

        // if (this.props.location.pathname.split('/').length > 4) {
        //     const id = this.props.location.pathname.split('/')[3];
        //     if (id != 'new')
        //         this.props.getAccount(id).then((data) => {
        //             this.setState({
        //                 id: data.id,
        //                 firstName: data.firstName,
        //                 lastName: data.lastName,
        //                 userName: data.userName,
        //                 email: data.email,
        //                 password: '1',
        //                 confirmPassword: '1',
        //                 submited: false,
        //                 editMode: true
        //             })
        //         });
        // }
    }
    
    render() {
        return (
            <div className="page-content-wrapper">
                <div className="page-content">
                    <div className="page-bar">
                        <ul className="page-breadcrumb">
                            <li>
                                <a href="index.html">خانه</a>
                                <i className="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">مدیریت کاربران</a>
                            </li>
                        </ul>
                    </div>
                    <h1 className="page-title">مدیریت کاربران</h1>
                    {/*<div className="note note-info">
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.   </p>{this.state.successProfile}
                    </div>*/}
                    <div className="row">
                        <div className="col-md-12">
                            {!!this.state.globalError ?
                                <div className="alert alert-danger alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>خطا!</strong> {this.state.globalError}
                                </div> : null}

                            {!!this.state.success ?
                                <div className="alert alert-success alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>موفقیت!</strong> {this.state.success}
                                </div> : null}
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="portlet light ">
                                <div className="portlet-title tabbable-line">
                                    <ul className="nav nav-tabs">
                                        <li className={this.props.location.pathname.indexOf('profile') > -1 == "" ? "" : "active"}>
                                            <Link className="nav-link nav-toggle" to={'/dashboard/account/' + this.props.location.pathname.split('/')[3] + '/profile'} >
                                                مشخصات فردی
                                            </Link>
                                        </li>

                                        {!!this.state.id ? <li className={this.props.location.pathname.indexOf('password') > -1 == "" ? "" : "active"}>
                                            <Link className="nav-link nav-toggle" to={'/dashboard/account/' + this.props.location.pathname.split('/')[3] + '/password'}>
                                                تغییر رمز عبور
                                            </Link>
                                        </li> : null
                                        }

                                        <li className={this.props.location.pathname.indexOf('roles') > -1 == "" ? "" : "active"}>
                                            <Link className="nav-link nav-toggle" to={'/dashboard/account/' + this.props.location.pathname.split('/')[3] + '/roles'}>
                                                مدیریت دسترسی
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                <div className="portlet-body">
                                    <div className="tab-content">
                                        <div className="tab-pane active" >
                                            <Route path={this.props.match.url + '/profile'} render={(props) => (<AccountForm accountSaved={(item) => { this.setState(item) }}  {...props} />)} />
                                            <Route path={this.props.match.url + '/password'} component={ChangePassword} />
                                            <Route path={this.props.match.url + '/roles'} render={(props) => (<AccountRoles roles={this.props.roles} accountSaved={(item) => { this.setState(item) }}  {...props} />)} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

function mapStateToProps(state) {
    return { roles: state.roles }
}

export default connect(mapStateToProps, { fetchRoles })(Account);