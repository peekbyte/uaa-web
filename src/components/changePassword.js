import React, { Component } from 'react'

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <form action="#">
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="control-label">رمز عبور فعلی</label>
                            <input type="password" className="form-control"></input>
                        </div>
                        <div className="form-group">
                            <label className="control-label">رمز عبور جدید</label>
                            <input type="password" className="form-control"></input>
                        </div>
                        <div className="form-group">
                            <label className="control-label">تکرار رمز عبور جدید</label>
                            <input type="password" className="form-control"></input>
                        </div>
                    </div>
                    <div className="col-md-6">

                        <div className="well profile-info">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را  طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود  نیاز شامل حروفچینی  استفاده قرار گیرد.</div>
                    </div>
                </div>

                <div className="btn-row">
                    <a href="javascript:;" className="btn green btn-form">ثبت  </a>
                    <a href="javascript:;" className="btn default btn-form">انصراف </a>
                </div>
            </form>
        )
    }
}