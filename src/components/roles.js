import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchRoles } from '../actions/roles'
import RoleList from './roleList'
import { deleteRole } from '../actions/role';

import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';

class Roles extends Component {

    constructor(props) {
        super(props);
        this.props.fetchRoles();
    }

    deleteRole(id) {
        if (window.confirm('برای حذف مطمئن هستید؟')) {
            debugger;
            this.props.deleteRole(id).then(() => this.props.fetchRoles());
        }
    }

    render() {

        return (
            <div className="page-content-wrapper">

                <div className="page-content">

                    <div className="page-bar">
                        <ul className="page-breadcrumb">
                            <li>
                                <a href="index.html">خانه</a>
                                <i className="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">مدیریت نقش ها</a>

                            </li>

                        </ul>

                    </div>

                    <h1 className="page-title">مدیریت نقش ها
            </h1>

                    {/*<div className="note note-info">
                        <p>؟؟؟؟.  </p>
                    </div>*/}
                    <div className="row">
                        <div className="col-md-12">
                            {/*
                            <div className="alert alert-success alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>موفقیت!</strong> عملیات با موفقیت انجام گردید.
                    </div>
                            <div className="alert alert-info alert-dismissable">
                                <button type="button" className="close" data-disrolesmiss="alert" aria-hidden="true"></button>
                                <strong>اطلاعات!</strong> شما دارای تعدادی پیغام خوانده نشده هستید.
                    </div>
                            <div className="alert alert-warning alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>هشدار!</strong> تعداد ویروس ها در حال افزایش است  .
                    </div>
                            <div className="alert alert-danger alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>خطا!</strong> عملیات ثبت نشد.
                    </div>*/}

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="portlet box blue-steel">
                                <div className="portlet-title">
                                    <div className="caption">
                                        <i className="fa icon-grid"></i>لیست نقش ها
                            </div>

                                    <div className="actions">
                                        {/*<button type="button" className="btn green btn-new-user" data-style="expand-up">
                                            <span className="ladda-label">
                                                <i className="icon-user-follow"></i>تعریف کاربر جدید</span>
                                            <span className="ladda-spinner"></span>
                                        </button>*/}

                                        <Link className="btn green btn-new-user" to={'role'}>
                                            <span className="ladda-label">
                                                <i className="icon-user-follow"></i>تعریف نقش جدید</span>
                                            <span className="ladda-spinner"></span>
                                        </Link>
                                    </div>
                                </div>
                                <div className="portlet-body">
                                    <div className="table-responsive">
                                        <RoleList deleteRole={this.deleteRole.bind(this)} roles={this.props.roles.roles}></RoleList>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        );
    }
}

function mapStateToProps(state) {
    return { roles: state.roles }
}

const mapDispatchToState = dispatch => {
    return {
        deleteRole: (id) => {
            return dispatch(deleteRole(id));
        },
        fetchRoles: (id) => {
            return dispatch(fetchRoles());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToState)(Roles);