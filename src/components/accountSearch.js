import React, { Component } from 'react'

export default class AccountSearch extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form >
                <div className="row">
                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label">نام</label>
                            <input type="text" className="form-control "></input>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label">نام خانوادگی</label>
                            <input type="text" className="form-control "></input>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label">نام کاربری</label>
                            <input type="text" className="form-control "></input>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label">شماره تلفن همراه</label>
                            <input type="text" className="form-control "></input>
                        </div>
                    </div>

                </div>
            </form>
        )
    }
}