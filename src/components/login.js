import React, { Component } from 'react'
import { connect } from 'react-redux'
import { auth } from '../actions/auth'
import { withRouter, browserHistory, Link } from "react-router-dom"
import './login.css'

class Login extends Component {

    constructor(props) {
        super(props);
    }

    doLogin(e) {
        let userName = this.refs.userName.value
        let password = this.refs.password.value
        this.props
            .doLogin(userName, password)
            .then((aaa) => { this.props.history.push("/") }, (error) => {
            })
    }

    render() {

        return (


            <div className="login">
                <div className="logo">
                    <img src="assets/img/logo-big.png" alt="" />
                </div>
                <div className="content">
                    <form className="login-form" onSubmit={(e) => { this.doLogin(e); e.preventDefault() }}>
                        <h3 className="form-title font-yellow-gold">ورود به سـامانه</h3>
                        <div className="alert alert-danger display-hide">
                            <button className="close" data-close="alert"></button>
                            <span> لطفاً پسورد و نام کاربری خود را وارد نمایید. </span>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">نام کاربری</label>
                            <div className="input-icon">
                                <i className="fa fa-user"></i>
                                <input className="form-control placeholder-no-fix" type="text" autoComplete="off" placeholder="نام کاربری" name="username" ref="userName" /> </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">رمز عبور</label>
                            <div className="input-icon">
                                <i className="fa fa-lock"></i>
                                <input className="form-control placeholder-no-fix" type="password" autoComplete="off" placeholder="رمز عبور" name="password" ref="password" /> </div>
                        </div>
                        <div className="form-actions">
                            <label className="rememberme check mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="remember" value="1" />به خاطر بسپار
                            <span></span>
                            </label>
                            {/*<a href="javascript:;" id="forget-password" className="forget-password">فراموشی رمز عبور؟ </a>*/}

                            <button type="submit" className="btn green uppercase login-btn">ورود </button>

                        </div>
                        {this.props.auth.error ?
                            <div>
                                <div className="alert alert-danger alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>خطا!</strong> نام کاربری یا کلمه عبور درست نیست.
                            </div>
                            </div> : null
                        }


                        <div className="create-account">
                            <p>
                                {/*<a href="javascript:;" id="register-btn" className="uppercase">ساخت حساب کاربری جدید</a>*/}
                            </p>
                        </div>
                    </form>
                </div>
                <div className="copyright"> 2014 © تمام حقوق برای شرکت رایان بورس محفوظ است. </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { auth: state.auth }
}


const mapDispatchToState = dispatch => {
    return {
        doLogin: (username, password) => {
            return dispatch(auth(username, password));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToState)(Login);