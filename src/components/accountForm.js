import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { createAccount } from '../actions/account';
import { editAccount } from '../actions/account';
import { getAccount } from '../actions/account';
import { editAccountRoles } from '../actions/account';


class AccountForm extends Component {
    constructor(props) {
        super(props);

        this.resetState();

        let id = null;

        if (this.props.location.pathname.split('/').length > 4) {
            id = this.props.location.pathname.split('/')[3];
            if (id != 'new')
                this.props.getAccount(id).then((data) => {
                    this.setState({
                        id: data.id,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        userName: data.userName,
                        email: data.email,
                        password: '1',
                        confirmPassword: '1',
                        submited: false,
                        editMode: true
                    })
                });
        }

    }

    resetState() {
        this.state = {
            userName: '',
            email: '',
            firstName: '',
            lastName: '',
            password: '',
            confirmPassword: '',
            submited: false,
            editMode: false,
            success: '',
            errors: {}
        }
    }

    handleChange(event) {
        let me = this;
        this.setState({ [event.target.name]: event.target.value }, () => {
            let errors = me.checkValidation();
            me.setState({ errors });
        })
    }

    checkValidation() {
        let errors = {};
        if (this.state.firstName === '') {
            errors.firstName = 'نام را وارد نمایید.';
        }
        if (this.state.lastName === '') {
            errors.lastName = 'نام خانوادگی را وارد نمایید.';
        }
        if (this.state.userName === '') {
            errors.userName = 'نام کاربری را وارد نمایید.';
        }
        if (this.state.email === '') {
            errors.email = 'ایمیل را وارد نمایید.';
        }

        if (this.state.password === '') {
            errors.password = 'کلمه عبور جدید را وارد نمایید.';
        }
        if (this.state.confirmPassword === '') {
            errors.confirmPassword = 'تکرار کلمه عبور جدید را وارد نمایید.';
        }
        if (this.state.password !== '' && this.state.confirmPassword !== '' && this.state.password !== this.state.confirmPassword) {
            errors.confirmPasswordNotMatch = 'کلمه عبور جدید  و تکرار کلمه عبور یکسان نمی باشد.';
        }
        return errors;
    }

    onSubmit(e) {
        e.preventDefault();

        this.setState({ globalError: '', success: '', submited: true });
        this.props.accountSaved({ globalError: '', success: '' });

        let errors = this.checkValidation();

        this.setState({ submitted: true, errors });

        if (Object.keys(errors).length === 0) {
            const { id, firstName, lastName, email, userName, password, confirmPassword } = this.state;

            if (!!id) {
                this.props.editAccount({ id, firstName, lastName, email, userName, password, confirmPassword }).then((data) => {
                    this.props.accountSaved({ success: 'کاربر با موفقیت ذخیره شد.' });
                    // this.props.history.push("/dashboard/roles")
                }, (result) => {
                    let errorMessage = '';
                    for (var property in result.response.data.modelState) {
                        if (result.response.data.modelState.hasOwnProperty(property)) {
                            errorMessage += result.response.data.modelState[property] + '. ';
                        }
                    }
                    this.props.accountSaved({ globalError: errorMessage });
                    this.resetState();
                });
            }
            else {
                this.props.createAccount({ firstName, lastName, email, userName, password, confirmPassword }).then((data) => {
                    this.props.accountSaved({ success: 'کاربر با موفقیت ذخیره شد.'});
                    this.resetState();
                    // this.props.history.push("/dashboard/roles")
                }, (result) => {
                    let errorMessage = '';
                    for (var property in result.response.data.modelState) {
                        if (result.response.data.modelState.hasOwnProperty(property)) {
                            errorMessage += result.response.data.modelState[property] + '. ';
                        }
                    }
                    this.props.accountSaved({ globalError: errorMessage })
                });
            }
        }


        // if (this.state.id) {
        //     const account = {
        //         Id: this.state.id,
        //         UserName: this.refs.userName.value,
        //         Email: this.refs.email.value,
        //         FirstName: this.refs.firstName.value,
        //         LastName: this.refs.lastName.value
        //     };

        //     this.props.editAccount(account).then(result => {
        //     },
        //         result => {
        //             for (var property in result.response.data.modelState) {
        //                 if (result.response.data.modelState.hasOwnProperty(property)) {
        //                     alert(result.response.data.modelState[property]);    // do stuff
        //                 }
        //             }
        //         }
        //     );
        // }

    }

    render() {
        return (
            <form onSubmit={this.onSubmit.bind(this)}>
                {this.state.success}
<div className="row">
                    <div className="col-sm-6 col-md-6">
                        <div className={classnames('form-group', { 'has-error': !!this.state.errors.firstName && this.state.submited })} >
                            <label className="control-label">نام</label>
                            <input type="text" name="firstName" maxLength="150" value={this.state.firstName} onChange={this.handleChange.bind(this)} className="form-control"></input>
                            {!!this.state.errors.firstName && this.state.submited ? <span className="help-block">{this.state.errors.firstName} </span> : null}
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6">
                        <div className={classnames('form-group', { 'has-error': !!this.state.errors.lastName && this.state.submited })} >
                            <label className="control-label">نام خانوادگی</label>
                            <input type="text" name="lastName" maxLength="150" ref="lastName" value={this.state.lastName} onChange={this.handleChange.bind(this)} className="form-control"></input>
                            {!!this.state.errors.lastName && this.state.submited ? <span className="help-block">{this.state.errors.lastName} </span> : null}
                        </div>
                    </div>
</div>
 <div className="row">
                    {/*<div className="col-sm-6 col-md-6">
                        <div className={this.firstNameHasError() ? "form-group has-error" : "form-group"} >
                            <label className="control-label">شماره تلفن همراه</label>
                            <input type="text" className="form-control"></input>
                        </div>

                    </div>*/}
                    <div className="col-sm-6 col-md-6">
                        <div className={classnames('form-group', { 'has-error': !!this.state.errors.userName && this.state.submited })} >
                            <label className="control-label">نام کاربری </label>
                            <input type="text" name="userName" maxLength="150" ref="userName" value={this.state.userName} onChange={this.handleChange.bind(this)} className="form-control"></input>
                            {!!this.state.errors.userName && this.state.submited ? <span className="help-block">{this.state.errors.userName} </span> : null}
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6">
                        <div className={classnames('form-group', { 'has-error': !!this.state.errors.email && this.state.submited })} >
                            <label className="control-label">ایمیل</label>
                            <input type="text" name="email" maxLength="150" ref="email" value={this.state.email} onChange={this.handleChange.bind(this)} className="form-control"></input>
                            {!!this.state.errors.email && this.state.submited ? <span className="help-block">{this.state.errors.email} </span> : null}
                        </div>
                    </div>
 </div>
 <div className="row">
                    {!this.state.editMode ? <div className="col-md-6  col-md-6">
                        <div className={classnames('form-group', { 'has-error': !!this.state.errors.password && this.state.submited })} >
                            <label className="control-label">رمز عبور</label>
                            <input type="password" name="password" maxLength="150" ref="password" className="form-control" value={this.state.password} onChange={this.handleChange.bind(this)}></input>
                            {!!this.state.errors.password && this.state.submited ? <span className="help-block">{this.state.errors.password} </span> : null}
                        </div>
                    </div> : null}

                    {!this.state.editMode ?
                        <div className="col-md-6  col-md-6">
                            <div className={classnames('form-group', { 'has-error': (!!this.state.errors.confirmPassword || !!this.state.errors.confirmPasswordNotMatch) && this.state.submited })} >
                                <label className="control-label">تکرار رمز عبور</label>
                                <input type="password" name="confirmPassword" maxLength="150" ref="confirmPassword" className="form-control" value={this.state.confirmPassword} onChange={this.handleChange.bind(this)}></input>
                                {!!this.state.errors.confirmPassword && this.state.submited ? <span className="help-block">{this.state.errors.confirmPassword} </span> : null}
                                {!!this.state.errors.confirmPasswordNotMatch && this.state.submited ? <span className="help-block">{this.state.errors.confirmPasswordNotMatch} </span> : null}
                            </div>
                        </div> : null}
 </div>

                <div className="btn-row">
                    <input type="submit" className="btn green btn-form" value="ثبت" />
                </div>
            </form>
        )
    }

}


function mapStateToProps(state) {
    return { account: state.account }
}


const mapDispatchToState = dispatch => {
    return {
        createAccount: (account) => {
            return dispatch(createAccount(account));
        },
        editAccount: (account) => {
            return dispatch(editAccount(account));
        },
        getAccount: (id) => {
            return dispatch(getAccount(id));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToState)(AccountForm);