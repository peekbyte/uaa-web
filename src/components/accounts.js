import React, { Component } from 'react'
import { connect } from "react-redux"
import { fetchAccounts } from '../actions/accounts'
import AccountList from './accountList'
import { deleteAccount } from '../actions/account';

import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';

class Accounts extends Component {

    constructor(props) {
        super(props);
        this.props.fetchAccounts();
        this.state = { userName: '', firstName: '', lastName: '' };
    }

    handleChange(event) {
        let propName = event.target.name;
        this.setState({ [propName]: event.target.value });
    }

    edit(item) {
        this.account = item;
        this.setState({ account: item })
    }

    doSearch() {
        this.props.fetchAccounts(this.state.userName, this.state.firstName, this.state.lastName);
    }

    clearFilters() {
        this.setState({ userName: '', firstName: '', lastName: '' }, this.doSearch);
    }

    deleteAccount(id) {
        if (window.confirm('برای حذف مطمئن هستید؟')) {
            debugger;
            this.props.deleteAccount(id).then(() => this.props.fetchAccounts());
        }
    }
    
    render() {

        return (
            <div className="page-content-wrapper">
                <div className="page-content">
                    <div className="page-bar">
                        <ul className="page-breadcrumb">
                            <li>
                                <a href="index.html">خانه</a>
                                <i className="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">مدیریت کاربران</a>
                            </li>
                        </ul>
                    </div>
                    <h1 className="page-title">مدیریت کاربران</h1>
                    {/*<div className="note note-info">
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.   </p>
                    </div>*/}
                    <div className="row">
                        <div className="col-md-12">
                            <div className="profile-content">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="portlet light bordered">
                                            <div className="portlet-title">
                                                <div className="caption">
                                                    <i className="fa icon-info font-green-sharp"></i>
                                                    <span className="caption-subject font-green-sharp bold uppercase">فرم کاربران </span>
                                                </div>

                                            </div>
                                            <div className="portlet-body">



                                                <form >
                                                    <div className="row">
                                                        <div className="col-sm-6 col-md-3">
                                                            <div className="form-group">
                                                                <label className="control-label">نام</label>
                                                                <input type="text" name="firstName" ref="firstName" onChange={this.handleChange.bind(this)} value={this.state.firstName} className="form-control "></input>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-3">
                                                            <div className="form-group">
                                                                <label className="control-label">نام خانوادگی</label>
                                                                <input type="text" name="lastName" ref="lastName" onChange={this.handleChange.bind(this)} value={this.state.lastName} className="form-control "></input>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-3">
                                                            <div className="form-group">
                                                                <label className="control-label">نام کاربری</label>
                                                                <input type="text" name="userName" ref="userName" onChange={this.handleChange.bind(this)} value={this.state.userName} className="form-control "></input>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-3">
                                                            {/*<div className="form-group">
                                                                <label className="control-label">شماره تلفن همراه</label>
                                                                <input type="text" className="form-control "></input>
                                                            </div>*/}
                                                        </div>

                                                    </div>
                                                </form>




                                               

                                            </div>
                                            <div class="form-actions">
				                         	 <div className="btn-row">
                                                    <a onClick={this.doSearch.bind(this)} className="btn green btn-form">جستجو  </a>
                                                    <a onClick={this.clearFilters.bind(this)} className="btn default btn-form">حذف فیلترها  </a>
                                                </div>
			                              	</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="portlet box blue-steel">
                                <div className="portlet-title">
                                    <div className="caption">
                                        <i className="fa icon-grid"></i>لیست کاربران
                            </div>
                                    <div className="actions">
                                        {/*<button type="button" className="btn green btn-new-user" data-style="expand-up">
                                            <span className="ladda-label">
                                                <i className="icon-user-follow"></i>تعریف کاربر جدید</span>
                                            <span className="ladda-spinner"></span>
                                        </button>*/}

                                        <Link className="btn green btn-new-user" to={'account/new/profile'}>
                                            <span className="ladda-label">
                                                <i className="icon-user-follow"></i>تعریف کاربر جدید</span>
                                            <span className="ladda-spinner"></span>
                                        </Link>
                                    </div>

                                </div>
                                <div className="portlet-body">
                                    <div className="table-responsive">
                                        <AccountList editHandler={this.edit.bind(this)} deleteAccount={this.deleteAccount.bind(this)} accounts={this.props.accounts.accounts}></AccountList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div >
        );
    }
}

function mapStateToProps(state) {
    return { accounts: state.accounts }
}

export default connect(mapStateToProps, { fetchAccounts, deleteAccount })(Accounts);