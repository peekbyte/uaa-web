import React, { Component } from 'react'

import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';

export default class RoleList extends Component {

    constructor(props) {
        super(props);
    }

    deleteItem() {
        window.alert('Alert!!!!!');
    }

    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th>نام  </th>
                        <th className="th-operation">عملیات </th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.roles.map((item) => {
                        return (
                            <tr>
                                <td> {item.name} </td>
                                <td className="btn-operation">
                                    
                                    <Link className="btn green-jungle" to={'role/' + item.id}>
                                        <span className="ladda-label">
                                            <i className="icon-note"></i> ویرایش</span>
                                        <span className="ladda-spinner"></span>
                                    </Link>

                                    <button type="button" onClick={() => { this.props.deleteRole(item.id) }} className="btn  red-soft " data-style="expand-up">
                                        <span className="ladda-label">
                                            <i className="icon-trash"></i> حذف</span>
                                        <span className="ladda-spinner"></span></button>
                                </td>
                            </tr>
                        )
                    }
                    )}
                </tbody>
            </table>
        )
    }
}