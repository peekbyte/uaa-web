import React, { Component } from 'react';
import { connect } from "react-redux";
import classnames from 'classnames';
import { changePassword, getProfile, editAccount } from '../actions/account';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            firstName: "",
            lastName: "",
            email: "",
            userName: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            submitted: false,
            success: false,
            errors: {},
            changePasswordErrors: {},
            globalError: ''
        };

        this.props.getProfile().then(data => {
            this.setState(data)
        });
    }

    handleChange(event) {
        let me = this;
        this.setState({ [event.target.name]: event.target.value }, () => {
            let errors = me.checkValidation();
            me.setState({ errors });
        })
    }

    handleChangePassword(event) {
        let me = this;
        this.setState({ [event.target.name]: event.target.value }, () => {
            let changePasswordErrors = me.checkPasswordValidation();
            me.setState({ changePasswordErrors });
        })
    }

    checkValidation() {
        let errors = {};
        if (this.state.firstName === '') {
            errors.firstName = 'نام را وارد نمایید.';
        }
        if (this.state.lastName === '') {
            errors.lastName = 'نام خانوادگی را وارد نمایید.';
        }
        if (this.state.userName === '') {
            errors.userName = 'نام کاربری را وارد نمایید.';
        }
        if (this.state.email === '') {
            errors.email = 'ایمیل را وارد نمایید.';
        }
        return errors;
    }

    checkPasswordValidation() {
        let errors = {};
        if (this.state.oldPassword === '') {
            errors.oldPassword = 'کلمه عبور جاری را وارد نمایید.';
        }
        if (this.state.newPassword === '') {
            errors.newPassword = 'کلمه عبور جدید را وارد نمایید.';
        }
        if (this.state.confirmPassword === '') {
            errors.confirmPassword = 'تکرار کلمه عبور جدید را وارد نمایید.';
        }
        if (this.state.newPassword !== '' && this.state.confirmPassword !== '' && this.state.newPassword !== this.state.confirmPassword) {
            errors.confirmPasswordNotMatch = 'کلمه عبور جدید  و تکرار کلمه عبور یکسان نمی باشد.';
        }
        return errors;
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({ globalError: '', success: false });

        let errors = this.checkValidation();

        this.setState({ submitted: true, errors });

        if (Object.keys(errors).length === 0) {
            const { id, firstName, lastName, email, userName } = this.state;

            this.props.editAccount({ id, firstName, lastName, email, userName }).then((data) => {
                this.setState({ success: true })
            }, (error) => {
                this.setState({ globalError: 'خطا!!!!' })
            });
        }
    }

    onPasswordSubmit(e) {
        e.preventDefault();
        this.setState({ globalError: '', success: false });

        let changePasswordErrors = this.checkPasswordValidation();

        this.setState({ submitted: true, changePasswordErrors });

        if (Object.keys(changePasswordErrors).length === 0) {
            const { id, oldPassword, newPassword, confirmPassword } = this.state;

            this.props.changePassword({ id, oldPassword, newPassword, confirmPassword }).then((data) => {
                this.setState({ success: true })
            }, (error) => {
                this.setState({ globalError: 'خطا!!!!' })
            });
        }
    }

    render() {
        return (
            <div className="page-content-wrapper">
                <div className="page-content">
                    <div className="page-bar">
                        <ul className="page-breadcrumb">
                            <li>
                                <a href="index.html">خانه</a>
                                <i className="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">تنظیمات حساب کاربری</a>
                            </li>
                        </ul>
                    </div>
                    <h1 className="page-title">تنظیمات حساب کاربری
                    </h1>
                    <div className="note note-info">
                        <p>لطفاً جهت استفاده از سامانه اطلاعات کاربری خود را کامل نمایید.  </p>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            {/*<div className="alert alert-success alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>موفقیت!</strong> عملیات با موفقیت انجام گردید.
                            </div>*/}

                            {/*<div className="alert alert-info alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>اطلاعات!</strong> شما دارای تعدادی پیغام خوانده نشده هستید.
                            </div>*/}
                            {/*
                            <div className="alert alert-danger alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>خطا</strong> تعداد ویروس ها در حال افزایش است .
                            </div>*/}

                            {/*{this.props.error ?
                                <div>
                                    <div className="alert alert-danger alert-dismissable">
                                        <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong>خطا!</strong> نام کاربری یا کلمه عبور درست نیست.
                            </div>
                                </div> : null
                            }*/}
                            {this.state.success ?
                                <div className="alert alert-success alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>موفقیت!</strong> عملیات با موفقیت انجام گردید.
                                </div> : null}

                            {!!this.state.globalError ?
                                <div className="alert alert-danger alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>خطا!</strong> {this.state.globalError}
                                </div> : null}

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="profile-sidebar">
                                <div className="portlet light profile-sidebar-portlet ">
                                    <div className="profile-userpic">

                                        <img src="../assets/img/profile_user.jpg" className="img-responsive" alt="" />

                                    </div>
                                    <div className="profile-usertitle">
                                        <div className="profile-usertitle-name">{this.state.fullName}</div>
                                        <div className="profile-usertitle-job">{this.state.userName}</div>
                                    </div>
                                    {/*<div className="profile-userbuttons">
                                        <button type="button" className="btn  blue-steel btn-sm">متن تستی</button>
                                        <button type="button" className="btn  red btn-sm">متن تستی</button>
                                    </div>*/}
                                    <div className="profile-usermenu">
                                        <ul className="nav">

                                            {/*<li className="active">
                                                <a href="page_user_profile_1_account.html">
                                                    <i className="icon-settings"></i>تنظیمات حساب کاربری </a>
                                            </li>*/}

                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div className="profile-content">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="portlet light ">
                                            <div className="portlet-title tabbable-line">

                                                <ul className="nav nav-tabs">
                                                    <li className="active">
                                                        <a href="#tab_1_1" data-toggle="tab">مشخصات فردی</a>
                                                    </li>

                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">تغییر رمز عبور</a>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div className="portlet-body">
                                                <div className="tab-content">
                                                    <div className="tab-pane active" id="tab_1_1">
                                                        <form onSubmit={this.onSubmit.bind(this)}>
                                                            <div className="row">
                                                                 <div className="col-sm-6 col-md-6">
                                                                <div className={classnames('form-group', { 'has-error': !!this.state.errors.firstName })}>
                                                                        <label className="control-label">نام</label>
                                                                        <input type="text" name="firstName" value={this.state.firstName} onChange={this.handleChange.bind(this)} className="form-control" />
                                                                        {!!this.state.errors.firstName ? <span className="help-block">{this.state.errors.firstName} </span> : null}
                                                                    </div>
                                                                </div>
                                                                <div className="col-sm-6 col-md-6">
                                                                    <div className={classnames('form-group', { 'has-error': !!this.state.errors.lastName })}>
                                                                        <label className="control-label">نام خانوادگی</label>
                                                                        <input type="text" name="lastName" value={this.state.lastName} onChange={this.handleChange.bind(this)} className="form-control" />
                                                                        {!!this.state.errors.lastName ? <span className="help-block">{this.state.errors.lastName} </span> : null}
                                                                    </div>
                                                                </div>
                                                              </div>
                                                              <div className="row">
                                                                {/*<div className="col-sm-6 col-md-6">
                                                                    <div className="form-group">
                                                                        <label className="control-label">شماره تلفن همراه</label>
                                                                        <input type="text" className="form-control" />
                                                                    </div>
                                                                </div>*/}
                                                                <div className="col-sm-6 col-md-6">
                                                                    <div className={classnames('form-group', { 'has-error': !!this.state.errors.userName })}>
                                                                        <label className="control-label">نام کاربری </label>
                                                                        <input type="text" name="userName" value={this.state.userName} onChange={this.handleChange.bind(this)} className="form-control" />
                                                                        {!!this.state.errors.userName ? <span className="help-block">{this.state.errors.userName} </span> : null}
                                                                    </div>
                                                                </div>
                                                                  <div className="col-sm-6 col-md-6">
                                                                    <div className={classnames('form-group', { 'has-error': !!this.state.errors.email })}>
                                                                        <label className="control-label">ایمیل</label>
                                                                        <input type="text" name="email" value={this.state.email} onChange={this.handleChange.bind(this)} className="form-control" />
                                                                        {!!this.state.errors.email ? <span className="help-block">{this.state.errors.email} </span> : null}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="btn-row">
                                                                <input type="submit" className="btn green btn-form" value="ثبت" />
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div className="tab-pane" id="tab_1_2">
                                                        <form onSubmit={this.onPasswordSubmit.bind(this)}>
                                                            <div className="row">
                                                                <div className="col-md-6">
                                                                    <div className={classnames('form-group', { 'has-error': !!this.state.changePasswordErrors.oldPassword })}>
                                                                        <label className="control-label">رمز عبور فعلی</label>
                                                                        <input type="password" name="oldPassword" className="form-control" />
                                                                        {!!this.state.changePasswordErrors.oldPassword ? <span className="help-block">{this.state.changePasswordErrors.oldPassword} </span> : null}
                                                                    </div>
                                                                    <div className={classnames('form-group', { 'has-error': !!this.state.changePasswordErrors.oldPassword })}>
                                                                        <label className="control-label">رمز عبور جدید</label>
                                                                        <input type="password" name="oldPassword" className="form-control" />
                                                                        {!!this.state.changePasswordErrors.newPassword ? <span className="help-block">{this.state.changePasswordErrors.newPassword} </span> : null}
                                                                    </div>
                                                                    <div className={classnames('form-group', { 'has-error': !!this.state.changePasswordErrors.confirmPassword || this.state.changePasswordErrors.confirmPasswordNotMatch })}>
                                                                        <label className="control-label">تکرار رمز عبور جدید</label>
                                                                        <input type="password" name="confirmPassword" className="form-control" />
                                                                        {!!this.state.changePasswordErrors.confirmPassword ? <span className="help-block">{this.state.changePasswordErrors.confirmPassword} </span> : null}
                                                                        {!!this.state.changePasswordErrors.confirmPasswordNotMatch ? <span className="help-block">{this.state.changePasswordErrors.confirmPasswordNotMatch} </span> : null}
                                                                    </div>
                                                                </div>
                                                                <div className="col-md-6">
                                                                    {/*<div className="well profile-info"></div>*/}
                                                                </div>
                                                            </div>
                                                            <div className="btn-row">
                                                                <input type="submit" className="btn green btn-form" value="ثبت" />
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { changePassword: state.changePassword, account: state.account }
}

export default connect(mapStateToProps, { getProfile, editAccount, changePassword })(Profile);