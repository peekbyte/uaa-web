import React, { Component } from 'react';
import { connect } from "react-redux";
import classnames from 'classnames';
import { createRole, getRole, editRole } from '../actions/role';
import { withRouter, browserHistory, Link } from "react-router-dom";


class Role extends Component {
    constructor(props) {
        super(props);
        let id = null;

        this.state = {
            id: '',
            name: '',
            submitted: false,
            errors: {},
            globalError: '',
            success: ''
        }

        if (this.props.location.pathname.split('/').length > 3) {
            id = this.props.location.pathname.split('/')[3];
            this.props.getRole(id).then((data) => {
                this.setState({
                    id: data.id,
                    name: data.name
                })
            });
        }
    }

    handleChange(event) {
        let me = this;
        this.setState({ [event.target.name]: event.target.value }, () => {
            let errors = me.checkValidation();
            me.setState({ errors });
        })
    }

    checkValidation() {
        let errors = {};
        if (this.state.name === '') {
            errors.name = 'نام را وارد نمایید.';
        }
        return errors;
    }

    onSubmit(e) {
        e.preventDefault();

        this.setState({ globalError: '', success: '' });

        let errors = this.checkValidation();

        this.setState({ submitted: true, errors });

        if (Object.keys(errors).length === 0) {
            const { id, name } = this.state;

            if (!!id) {
                this.props.editRole({ id, name }).then((data) => {
                    this.setState({ success: 'نقش با موفقیت ذخیره شد.' })
                    // this.props.history.push("/dashboard/roles")
                }, (result) => {
                    let errorMessage = '';
                    for (var property in result.response.data.modelState) {
                        if (result.response.data.modelState.hasOwnProperty(property)) {
                            errorMessage += result.response.data.modelState[property] + '. ';
                        }
                    }
                    this.setState({ globalError: errorMessage })
                });
            }
            else {

                this.props.createRole({ name }).then((data) => {
                    this.setState({ success: 'نقش با موفقیت ذخیره شد.', name: '' })
                    // this.props.history.push("/dashboard/roles")
                }, (result) => {
                    let errorMessage = '';
                    for (var property in result.response.data.modelState) {
                        if (result.response.data.modelState.hasOwnProperty(property)) {
                            errorMessage += result.response.data.modelState[property] + '. ';
                        }
                    }
                    this.setState({ globalError: errorMessage });
                });
            }
        }
    }

    render() {
        return (
            <div className="page-content-wrapper">

                <div className="page-content">

                    <div className="page-bar">
                        <ul className="page-breadcrumb">
                            <li>
                                <a href="index.html">خانه</a>
                                <i className="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">مدیریت نقش ها</a>

                            </li>

                        </ul>

                    </div>

                    <h1 className="page-title">مدیریت نقش ها</h1>

                    {/*<div className="note note-info">
                        <p>؟؟؟؟.  </p>
                    </div>*/}
                    <div className="row">
                        <div className="col-md-12">
                            {/*
                            <div className="alert alert-success alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>موفقیت!</strong> عملیات با موفقیت انجام گردید.
                    </div>
                            <div className="alert alert-info alert-dismissable">
                                <button type="button" className="close" data-disrolesmiss="alert" aria-hidden="true"></button>
                                <strong>اطلاعات!</strong> شما دارای تعدادی پیغام خوانده نشده هستید.
                    </div>
                            <div className="alert alert-warning alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>هشدار!</strong> تعداد ویروس ها در حال افزایش است  .
                    </div>
                            <div className="alert alert-danger alert-dismissable">
                                <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                <strong>خطا!</strong> عملیات ثبت نشد.
                    </div>*/}
                            {!!this.state.globalError ?
                                <div className="alert alert-danger alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>خطا!</strong> {this.state.globalError}
                                </div> : null}

                            {!!this.state.success ?
                                <div className="alert alert-success alert-dismissable">
                                    <button type="button" className="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>موفقیت!</strong> {this.state.success}
                                </div> : null}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="portlet box blue">
                                <div className="portlet-title">
                                    <div className="caption">
                                        <i className="fa icon-grid"></i>ویرایش نقش
                                    </div>
                                </div>
                                <div className="portlet-body">
                                    <form onSubmit={this.onSubmit.bind(this)}>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className={classnames('form-group', { 'has-error': !!this.state.errors.name })} >
                                                    <label className="control-label">نام</label>
                                                    <input type="text" name="name" maxLength="150" value={this.state.name} onChange={this.handleChange.bind(this)} className="form-control"></input>
                                                    {!!this.state.errors.name ? <span className="help-block">{this.state.errors.name} </span> : null}
                                                </div>
                                                {/*<div className="form-group">
                                                    <label className="control-label">ارزش</label>
                                                    <input type="text" className="form-control"></input>
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">انتخاب سامانه</label>
                                                    <select className="mt-multiselect btn btn-default" multiple="multiple" data-width="100%">
                                                        <option value="cheese">Cheese</option>
                                                        <option value="tomatoes">Tomatoes</option>
                                                        <option value="mozarella">Mozzarella</option>
                                                        <option value="mushrooms">Mushrooms</option>
                                                        <option value="pepperoni">Pepperoni</option>
                                                        <option value="onions">Onions</option>
                                                    </select>
                                                </div>*/}
                                            </div>
                                        </div>
                                        <div className="btn-row">
                                            <input type="submit" className="btn green btn-form" value="ثبت" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { role: state.role }
}

export default connect(mapStateToProps, { createRole, getRole, editRole })(Role);