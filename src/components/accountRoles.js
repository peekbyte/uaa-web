import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { getAccount } from '../actions/account';
import { editAccountRoles, getAccountRoles } from '../actions/account';

class AccountRoles extends Component {
    constructor(props) {
        super(props);
        this.state = { id: '', roles: [] }

        let id = null;

        if (this.props.location.pathname.split('/').length > 4) {
            id = this.props.location.pathname.split('/')[3];
            if (id != 'new')
                this.props.getAccountRoles(id).then((data) => {
                    this.setState({ id, roles: data })
                });
        }
    }

    componentDidMount() {
    }

    onSubmit(e) {
        e.preventDefault();
        let roles = this.state.roles.filter(x => x.hasRole).map(x => { return x.name; });
        this.props.editAccountRoles(this.state.id, roles).then((data) => {
            this.props.accountSaved({ success: 'نقش با موفقیت ذخیره شد.' });
        }, (result) => {
            this.props.accountSaved({ globalError: 'خطا' });
        });;

        // this.setState({ globalError: '', success: false });
        // this.props.accountSaved({ globalError: '', success: false });

        // let errors = this.checkValidation();

        // this.setState({ submitted: true, errors });

        // if (Object.keys(errors).length === 0) {
        //     const { id, firstName, lastName, email, userName, password, confirmPassword } = this.state;

        //     if (!!id) {
        //         this.props.editAccount({ id, firstName, lastName, email, userName, password, confirmPassword }).then((data) => {
        //             this.props.accountSaved({ success: 'کاربر با موفقیت ذخیره شد.' });
        //             // this.props.history.push("/dashboard/roles")
        //         }, (result) => {
        //             let errorMessage = '';
        //             for (var property in result.response.data.modelState) {
        //                 if (result.response.data.modelState.hasOwnProperty(property)) {
        //                     errorMessage += result.response.data.modelState[property] + '. ';
        //                 }
        //             }
        //             this.props.accountSaved({ globalError: errorMessage });
        //             this.resetState();
        //         });
        //     }
        //     else {
        //         this.props.createAccount({ firstName, lastName, email, userName, password, confirmPassword }).then((data) => {
        //             this.props.accountSaved({ success: 'کاربر با موفقیت ذخیره شد.' });
        //             this.resetState();
        //             // this.props.history.push("/dashboard/roles")
        //         }, (result) => {
        //             let errorMessage = '';
        //             for (var property in result.response.data.modelState) {
        //                 if (result.response.data.modelState.hasOwnProperty(property)) {
        //                     errorMessage += result.response.data.modelState[property] + '. ';
        //                 }
        //             }
        //             this.props.accountSaved({ globalError: errorMessage })
        //         });
        //     }
        // }
    }

    handleChange(event) {
        let me = this;
        let { roles } = this.state;
        let objIndex = roles.findIndex(x => { return x.id == event.target.name });
        roles[objIndex].hasRole = event.target.value == "checked";
        this.setState({ roles }, () => {
        })
    }

    render() {
        return (
            <form onSubmit={this.onSubmit.bind(this)}>
                <table className="table table-light table-hover access-management">
                    <tbody>
                        {this.state.roles.map((item) => {

                            return (
                                <tr key={item.id}>
                                    <td>{item.name}</td>
                                    <td>
                                        <div className="checkbox">
                                            <label>
                                                <input type="checkbox" value="" />سطح دسترسی</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="mt-radio-inline">
                                            <label className="mt-radio">
                                                <input type="radio" name={item.id} onChange={this.handleChange.bind(this)} value="checked" checked={item.hasRole} />
                                                بله
                                                                                    <span></span>
                                            </label>
                                            <label className="mt-radio">
                                                <input type="radio" name={item.id} onChange={this.handleChange.bind(this)} value="unChecked" checked={!item.hasRole} />
                                                خیر
                                                                                    <span></span>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            )
                        }
                        )}

                    </tbody>
                </table>
                <div className="btn-row">
                    <input type="submit" className="btn green btn-form" value="ثبت" />
                </div>
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps, { editAccountRoles, getAccount, getAccountRoles })(AccountRoles);