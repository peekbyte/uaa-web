import React, { Component } from 'react'
import { connect } from 'react-redux'
import { auth } from '../actions/auth'
import { withRouter, browserHistory, Link } from "react-router-dom"

class ForgotPassword extends Component {

    constructor(props) {
        super(props);
    }


    // doLogin(event) {

    //     event.preventDefault()
    //     debugger;
    //     let userName = this.refs.userName.value
    //     let password = this.refs.password.value


    // }


    // doLogin(e) {
    //     e.preventDefault();
    //     // this.setState({
    //     //     ...this.state,
    //     //     emailError: this.state.email.length === 0 ? "Email cannot be blank" : "",
    //     //     passwordError: (
    //     //         this.state.password.length === 0 ? "Password cannot be blank" : ""
    //     //     )
    //     // });


    // }

    /*<div>
                <div>
                    {this.props.auth.token.access_token}
                </div>
                <form >
                    <input type="text" placeholder="Username" ref="userName" />
                    <input type="password" placeholder="Password" ref="password" />
                    <Button></Button>
                </form>
            </div>*/


    render() {
        console.log(this.props)
        let me = this;
        const Button = withRouter(({ history }) => (
            <button
                type='button'
                onClick={() => {

                    let userName = this.refs.userName.value
                    let password = this.refs.password.value

                    debugger;

                    me.props
                        .doLogin(userName, password)
                        .then((aaa) => { history.push("/accounts") });
                }} >
                Click Me!
        </button>
        ))

        return (
            <div className="login">
                <div className="logo">
                    <a href="index.html">
                        <img src="assets/img/logo-big.png" alt="" /> </a>
                </div>
                <div className="content">
                    <form className="forget-form" action="index.html" method="post">
                        <h3 className="font-yellow-gold">فراموشی رمز عبور </h3>
                        <p> آدرس ایمیل خود را وارد نمایید تا رمز عبور خود را ریست نمایید. </p>
                        <div className="form-group">
                            <div className="input-icon">
                                <i className="fa fa-envelope"></i>
                                <input className="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="ایمیل" name="email" /> </div>
                        </div>
                        <div className="form-actions">
                            <button type="button" id="back-btn" className="btn green btn-outline">بازگشت</button>
                            <button type="submit" className="btn btn-success uppercase pull-right">ارسال</button>
                        </div>
                    </form>
                </div>
                <div className="copyright"> 2014 © تمام حقوق برای شرکت رایان بورس محفوظ است. </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { auth: state.auth }
}


const mapDispatchToState = dispatch => {
    return {
        doLogin: (username, password) => {
            debugger;
            return dispatch(auth(username, password));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToState)(ForgotPassword);