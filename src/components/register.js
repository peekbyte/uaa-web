import React, { Component } from 'react'
import { connect } from 'react-redux'
import { auth } from '../actions/auth'
import { withRouter, browserHistory, Link } from "react-router-dom"

class Register extends Component {

    constructor(props) {
        super(props);
    }


    // doLogin(event) {

    //     event.preventDefault()
    //     debugger;
    //     let userName = this.refs.userName.value
    //     let password = this.refs.password.value


    // }


    // doLogin(e) {
    //     e.preventDefault();
    //     // this.setState({
    //     //     ...this.state,
    //     //     emailError: this.state.email.length === 0 ? "Email cannot be blank" : "",
    //     //     passwordError: (
    //     //         this.state.password.length === 0 ? "Password cannot be blank" : ""
    //     //     )
    //     // });


    // }

    /*<div>
                <div>
                    {this.props.auth.token.access_token}
                </div>
                <form >
                    <input type="text" placeholder="Username" ref="userName" />
                    <input type="password" placeholder="Password" ref="password" />
                    <Button></Button>
                </form>
            </div>*/


    render() {
        console.log(this.props)
        let me = this;
        const Button = withRouter(({ history }) => (
            <button
                type='button'
                onClick={() => {

                    let userName = this.refs.userName.value
                    let password = this.refs.password.value

                    debugger;

                    me.props
                        .doLogin(userName, password)
                        .then((aaa) => { history.push("/accounts") });
                }} >
                Click Me!
        </button>
        ))

        return (


            <div className="login">
                <div className="logo">
                    <a href="index.html">
                        <img src="assets/img/logo-big.png" alt="" /> </a>
                </div>
                <div className="content">
                    <form className="register-form" action="index.html" method="post">
                        <h3 className="font-yellow-gold">ساخت حساب کاربری جدید</h3>
                        <p className="hint"> اطلاعات خود را در زیر وارد نمایید: </p>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">نام کامل</label>
                            <div className="input-icon">
                                <i className="fa fa-font"></i>
                                <input className="form-control placeholder-no-fix" type="text" placeholder="نام کامل" name="fullname" /> </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">ایمیل</label>
                            <div className="input-icon">
                                <i className="fa fa-envelope"></i>
                                <input className="form-control placeholder-no-fix" type="text" placeholder="ایمیل" name="email" /> </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">آدرس</label>
                            <div className="input-icon">
                                <i className="fa fa-check"></i>
                                <input className="form-control placeholder-no-fix" type="text" placeholder="آدرس" name="address" /> </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">شهر</label>
                            <div className="input-icon">
                                <i className="fa fa-location-arrow"></i>
                                <input className="form-control placeholder-no-fix" type="text" placeholder="شهر" name="city" /> </div>
                        </div>

                        <p className="hint"> اطلاعات حساب خود را در زیر وارد نمایید: </p>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">نام کاربری</label>
                            <div className="input-icon">
                                <i className="fa fa-user"></i>
                                <input className="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="نام کاربری" name="username" /> </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">پسورد</label>
                            <div className="input-icon">
                                <i className="fa fa-lock"></i>
                                <input className="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="پسورد" name="password" />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label visible-ie8 visible-ie9">تکرار پسورد</label>
                            <div className="controls">
                                <div className="input-icon">
                                    <i className="fa fa-check"></i>
                                    <input className="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="تکرار پسورد" name="rpassword" /> </div>
                            </div>
                        </div>
                        <div className="form-group margin-top-20 margin-bottom-20">
                            <label className="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="tnc" /> با تمامی شرایط ثبت نام موافق هستم
                            <span></span>
                            </label>
                            <div id="register_tnc_error"> </div>
                        </div>
                        <div className="form-actions">
                            <button type="button" id="register-back-btn" className="btn green btn-outline">بازگشت</button>
                            <button type="submit" id="register-submit-btn" className="btn btn-success uppercase pull-right">ارسال</button>
                        </div>
                    </form>
                </div>
                <div className="copyright"> 2014 © تمام حقوق برای شرکت رایان بورس محفوظ است. </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { auth: state.auth }
}


const mapDispatchToState = dispatch => {
    return {
        doLogin: (username, password) => {
            return dispatch(auth(username, password));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToState)(Register);