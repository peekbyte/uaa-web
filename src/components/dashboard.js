import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchAccounts } from '../actions/accounts'

import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';


import Accounts from './accounts';
import Account from './account';
import Roles from './roles';
import Profile from './profile';
import SideBar from '../containers/sideBar'
import NavBar from '../containers/navBar'
import Footer from '../containers/footer'

import Role from './role'

export default class Dashboard extends Component {

    constructor(props) {

        super(props);
        // this.props.dispatch(fetchAccounts());
    }

    render() {
        console.log(this.props)

        return (
            <div className="page-wrapper">

                <NavBar></NavBar>

                <div className="clearfix"></div>
                <div className="page-container">

                    <Route path={this.props.match.url + '/'} component={SideBar} />

                    <Route path={this.props.match.url + '/myprofile'} component={Profile} />

                    <Route path={this.props.match.url + '/accounts'} component={Accounts} />

                    <Route path={this.props.match.url + '/account/:id?'} component={Account} />

                    <Route path={this.props.match.url + '/roles'} component={Roles} />

                    <Route path={this.props.match.url + '/role/:id?'} component={Role} />

                </div>
                <Footer></Footer>
            </div>
        );
    }
}
