import { combineReducers } from 'redux'
import accountsReducer from './reducers/accounts'
import authReducer from './reducers/auth'
import changePassword from './reducers/changePassword'
import getAccount from './reducers/getAccount'
import createAccount from './reducers/createAccount'

import getRole from './reducers/getRole'
import createRole from './reducers/createRole'
import rolesReducer from './reducers/roles'

import deleteAccount from './reducers/deleteAccount'
import editAccountRoles from './reducers/editAccountRoles'
import getAccountRoles from './reducers/getAccountRoles'

export default combineReducers({
    // account: accountReducer,
    accounts: accountsReducer,
    roles: rolesReducer,
    auth: authReducer,
    changePassword: changePassword,
    account: getAccount,
    createAccount: createAccount,
    role: getRole,
    createRole: createRole,
    deleteAccount: deleteAccount,
    editAccountRoles: editAccountRoles,
    accountRoles: getAccountRoles
})
