import React, { Component } from 'react';
import { connect } from "react-redux";
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link
} from 'react-router-dom';

export default class SideBar extends Component {

    constructor(props) {
        super(props);
    }

    navLink(title, name, icon, group) {
        return <li className={this.props.location.pathname.indexOf(group) > -1 ? "nav-item   active open" : "nav-item"} >
            <Link className="nav-link nav-toggle" to={"/dashboard/" + name}>
                <i className={icon}></i>
                <span className="title">{title}</span>
                {this.props.location.pathname.indexOf(group) > -1 ? <span className="selected"></span> : null}
            </Link>
        </li>
    }

    render() {
        return (
            <div className="page-sidebar-wrapper">
                <div className="page-sidebar navbar-collapse collapse">
                    <ul className="page-sidebar-menu page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        {this.navLink('تنظیمات حساب کاربری', 'myprofile', 'icon-settings', 'myprofile')}
                        {this.navLink('مدیریت کاربران', 'accounts', 'icon-lock', 'account')}
                        {this.navLink('مدیریت نقش ها', 'roles', 'icon-users', 'role')}
                    </ul>
                </div>
            </div>
        );
    }
}
