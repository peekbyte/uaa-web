import React, { Component } from 'react';
import { connect } from "react-redux";

export default class Footer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props)

        return (
            <div className="page-footer">
                <div className="page-footer-inner">
                    کلیه حقوق این سامانه متعلق به
                 <a href="http://rayanbourse.ir/" title="رایان بورس" target="_blank">شرکت رایان بورس </a> میباشد | 2016 &copy;
             </div>
                <div className="scroll-to-top">
                    <i className="icon-arrow-up"></i>
                </div>
            </div>
        );
    }
}

// function mapStateToProps(state) {
//     return { accounts: state.accounts }
// }
