import React, { Component } from 'react'
import { connect } from "react-redux"
import logo from '../images/logo.png'
import avatar3_small from '../images/avatar3_small.jpg'
import { withRouter } from 'react-router-dom'

export default class NavBar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props)

        const SignOut = withRouter(({ history }) => (

            <a onClick={() => {
                sessionStorage.removeItem('access-token');
                history.push('/')
            }} >
                <i className="icon-key"></i>خروج از سیستم
                </a>
        ))

        return (
            <div className="page-header navbar navbar-fixed-top">
                <div className="page-header-inner ">
                    <div className="page-logo">
                        <a href="index.html">
                            <img src={logo} alt="logo" className="logo-default" />
                        </a>
                        <div className="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <a href="javascript:;" className="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <div className="top-menu">
                        <ul className="nav navbar-nav pull-right">
                            <li className="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i className="icon-home"></i>
                                </a>
                            </li>
                            <li className="dropdown dropdown-user">
                                <a href="javascript:;" className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" className="img-circle" src={avatar3_small.jpg} />
                                    <span className="username username-hide-on-mobile">نام کاربر </span>
                                    <i className="fa fa-angle-down"></i>
                                </a>
                                <ul className="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="page_user_profile_1.html">
                                            <i className="icon-user"></i>پروفایل من</a>
                                    </li>
                                    <li>
                                        <SignOut />
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

// function mapStateToProps(state) {
//     return { accounts: state.accounts }
// }
